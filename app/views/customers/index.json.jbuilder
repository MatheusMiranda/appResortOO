json.array!(@customers) do |customer|
  json.extract! customer, :id, :name, :cpf
  json.url customer_url(customer, format: :json)
end
