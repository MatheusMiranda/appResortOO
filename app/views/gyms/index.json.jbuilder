json.array!(@gyms) do |gym|
  json.extract! gym, :id, :date, :idUser
  json.url gym_url(gym, format: :json)
end
