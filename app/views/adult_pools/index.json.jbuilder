json.array!(@adult_pools) do |adult_pool|
  json.extract! adult_pool, :id, :date, :idUser
  json.url adult_pool_url(adult_pool, format: :json)
end
