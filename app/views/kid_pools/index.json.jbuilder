json.array!(@kid_pools) do |kid_pool|
  json.extract! kid_pool, :id, :date, :idUser
  json.url kid_pool_url(kid_pool, format: :json)
end
