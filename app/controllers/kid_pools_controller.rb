class KidPoolsController < ApplicationController
  before_action :set_kid_pool, only: [:show, :edit, :update, :destroy]

  # GET /kid_pools
  # GET /kid_pools.json
  def index
    @kid_pools = KidPool.all
  end

  # GET /kid_pools/1
  # GET /kid_pools/1.json
  def show
  end

  # GET /kid_pools/new
  def new
    @kid_pool = KidPool.new
  end

  # GET /kid_pools/1/edit
  def edit
  end

  # POST /kid_pools
  # POST /kid_pools.json
  def create
    @kid_pool = KidPool.new(kid_pool_params)

    respond_to do |format|
      if @kid_pool.save
        format.html { redirect_to @kid_pool, notice: 'Kid pool was successfully created.' }
        format.json { render :show, status: :created, location: @kid_pool }
      else
        format.html { render :new }
        format.json { render json: @kid_pool.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /kid_pools/1
  # PATCH/PUT /kid_pools/1.json
  def update
    respond_to do |format|
      if @kid_pool.update(kid_pool_params)
        format.html { redirect_to @kid_pool, notice: 'Kid pool was successfully updated.' }
        format.json { render :show, status: :ok, location: @kid_pool }
      else
        format.html { render :edit }
        format.json { render json: @kid_pool.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /kid_pools/1
  # DELETE /kid_pools/1.json
  def destroy
    @kid_pool.destroy
    respond_to do |format|
      format.html { redirect_to kid_pools_url, notice: 'Kid pool was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_kid_pool
      @kid_pool = KidPool.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def kid_pool_params
      params.require(:kid_pool).permit(:date, :idUser)
    end
end
