class AdultPoolsController < ApplicationController
  before_action :set_adult_pool, only: [:show, :edit, :update, :destroy]

  # GET /adult_pools
  # GET /adult_pools.json
  def index
    @adult_pools = AdultPool.all
  end

  # GET /adult_pools/1
  # GET /adult_pools/1.json
  def show
  end

  # GET /adult_pools/new
  def new
    @adult_pool = AdultPool.new
  end

  # GET /adult_pools/1/edit
  def edit
  end

  # POST /adult_pools
  # POST /adult_pools.json
  def create
    @adult_pool = AdultPool.new(adult_pool_params)

    respond_to do |format|
      if @adult_pool.save
        format.html { redirect_to @adult_pool, notice: 'Adult pool was successfully created.' }
        format.json { render :show, status: :created, location: @adult_pool }
      else
        format.html { render :new }
        format.json { render json: @adult_pool.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /adult_pools/1
  # PATCH/PUT /adult_pools/1.json
  def update
    respond_to do |format|
      if @adult_pool.update(adult_pool_params)
        format.html { redirect_to @adult_pool, notice: 'Adult pool was successfully updated.' }
        format.json { render :show, status: :ok, location: @adult_pool }
      else
        format.html { render :edit }
        format.json { render json: @adult_pool.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /adult_pools/1
  # DELETE /adult_pools/1.json
  def destroy
    @adult_pool.destroy
    respond_to do |format|
      format.html { redirect_to adult_pools_url, notice: 'Adult pool was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_adult_pool
      @adult_pool = AdultPool.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def adult_pool_params
      params.require(:adult_pool).permit(:date, :idUser)
    end
end
