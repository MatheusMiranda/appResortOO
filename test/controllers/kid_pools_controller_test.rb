require 'test_helper'

class KidPoolsControllerTest < ActionController::TestCase
  setup do
    @kid_pool = kid_pools(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:kid_pools)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create kid_pool" do
    assert_difference('KidPool.count') do
      post :create, kid_pool: { date: @kid_pool.date, idUser: @kid_pool.idUser }
    end

    assert_redirected_to kid_pool_path(assigns(:kid_pool))
  end

  test "should show kid_pool" do
    get :show, id: @kid_pool
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @kid_pool
    assert_response :success
  end

  test "should update kid_pool" do
    patch :update, id: @kid_pool, kid_pool: { date: @kid_pool.date, idUser: @kid_pool.idUser }
    assert_redirected_to kid_pool_path(assigns(:kid_pool))
  end

  test "should destroy kid_pool" do
    assert_difference('KidPool.count', -1) do
      delete :destroy, id: @kid_pool
    end

    assert_redirected_to kid_pools_path
  end
end
