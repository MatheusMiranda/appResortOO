class CreateKidPools < ActiveRecord::Migration
  def change
    create_table :kid_pools do |t|
      t.datetime :date
      t.string :idUser

      t.timestamps null: false
    end
  end
end
