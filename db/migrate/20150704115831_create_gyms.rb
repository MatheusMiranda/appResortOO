class CreateGyms < ActiveRecord::Migration
  def change
    create_table :gyms do |t|
      t.datetime :date
      t.string :idUser

      t.timestamps null: false
    end
  end
end
