class CreateSaunas < ActiveRecord::Migration
  def change
    create_table :saunas do |t|
      t.datetime :date
      t.string :idUser

      t.timestamps null: false
    end
  end
end
