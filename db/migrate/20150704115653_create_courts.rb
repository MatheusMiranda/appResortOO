class CreateCourts < ActiveRecord::Migration
  def change
    create_table :courts do |t|
      t.datetime :date
      t.string :idUser

      t.timestamps null: false
    end
  end
end
